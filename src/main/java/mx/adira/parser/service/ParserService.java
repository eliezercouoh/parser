package mx.adira.parser.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;

@Service
public class ParserService {


    public ResponseEntity<JSONArray> parserFactura(MultipartFile file, String nodoName, ArrayList<String> attributes){
        ResponseEntity<JSONArray> response = new ResponseEntity<>(HttpStatus.OK);
        JSONArray parserJson = new JSONArray();
        try{
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
            Document document = documentBuilder.parse(file.getInputStream());
            document.getDocumentElement().normalize();

            NodeList elementsNode = document.getElementsByTagName(nodoName);

            if (attributes!=null){
                for (int i = 0; i < elementsNode.getLength(); i++) {
                    Node node = elementsNode.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        JSONObject jsonObject = new JSONObject();
                        Element element = (Element) node ;
                        for (String atributo : attributes) {
                            jsonObject.put(atributo, element.getAttribute(atributo));
                        }
                        parserJson.add(jsonObject);
                    }
                }
            }
            else {
                parserJson.add(parserGeneral(elementsNode));
            }


            response = ResponseEntity.status(HttpStatus.OK).body(parserJson);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            response = ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    private static JSONArray parserGeneral(NodeList nodeList) {
        JSONArray dataArr = new JSONArray();
        JSONObject dataObject = new JSONObject();
        for (int count = 0; count < nodeList.getLength(); count++) {
            Node tempNode = nodeList.item(count);
            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

                JSONObject dataAttributes = new JSONObject();
                Element elementE1 = (Element) tempNode;
                NamedNodeMap nnm = elementE1.getAttributes();
                for (int i = 0; i < nnm.getLength(); i++) {
                    dataAttributes.put(nnm.item(i).getNodeName(),nnm.item(i).getNodeValue());
                }
                JSONArray jsArrAtri = new JSONArray();
                jsArrAtri.add(dataAttributes);

                if (tempNode.hasChildNodes() && tempNode.getChildNodes().getLength() > 1) {
                    JSONArray temArr = parserGeneral(tempNode.getChildNodes());
                    if (dataObject.containsKey(tempNode.getNodeName())) {
                        dataObject.getJSONArray(tempNode.getNodeName()).add(temArr.getJSONObject(0));
                    } else {
                        dataObject.put(tempNode.getNodeName(), temArr);
                    }
                    if(!dataAttributes.isEmpty()){
                        temArr.getJSONObject(0).put("attributes",jsArrAtri);
                    }
                } else {
                    if(!dataAttributes.isEmpty()){
                        JSONArray temJA = new JSONArray();
                        JSONObject temJO = new JSONObject();
                        temJO.put("attributes", jsArrAtri);
                        temJA.add(temJO);
                        dataObject.put(tempNode.getNodeName(), temJA);
                    }else {
                        dataObject.put(tempNode.getNodeName(),tempNode.getTextContent());
                    }
                }
            }
        }
        dataArr.add(dataObject);
        return dataArr;
    }

}
