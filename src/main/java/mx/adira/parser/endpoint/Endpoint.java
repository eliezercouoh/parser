package mx.adira.parser.endpoint;

import mx.adira.parser.service.ParserService;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Arrays;


@RestController
public class Endpoint {

    @Autowired
    ParserService parserService;

    @RequestMapping(value = "/parser", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<JSONArray> parserFactura(
            @RequestParam(value = "file") MultipartFile file,
            @RequestParam(value = "Nodo") String nodo,
            @RequestParam(value = "Atributo", required = false) ArrayList<String> attributes
            ){
        return parserService.parserFactura(file,nodo,attributes);
    }

}
